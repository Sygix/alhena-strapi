export default {
    /**
     * Simple example.
     * Every 2 minutes.
     */
    orderCheck: {
    task: async ({ strapi }) => {
        try {
          await strapi.service('api::order.order').clean(new Date().getMinutes() - 1);
        } catch (error) {
          console.log(`task error : ${error}`);
        }
      },
      options: {
        rule: "*/2 * * * *",
      },
    },
  };