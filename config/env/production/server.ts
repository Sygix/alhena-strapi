import cronTasks from "./cron-tasks";

export default ({ env }) => ({
  proxy: true,
  url: env('PUBLIC_URL'),
  app: {
    keys: env.array('APP_KEYS'),
  },
  webhooks: {
    populateRelations: env.bool('WEBHOOKS_POPULATE_RELATIONS', false),
  },
  cron: {
    enabled: env.bool('CRON_ENABLED', false),
    tasks: cronTasks
  },
});
