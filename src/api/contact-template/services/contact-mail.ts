/**
 * contact-mail service
 */

import { factories } from "@strapi/strapi";
import utils from '@strapi/utils';
import _ from "lodash";

const { ApplicationError } = utils.errors;


export default factories.createCoreService('api::contact-template.contact-template', ({ strapi }) => ({
  sendmail: async (locale, data: {
    name: string;
    email: string;
    subject: string;
    message: string;
    consent: true;
  }) => {
    try {
      const templates: any = await strapi.entityService.findMany('api::contact-template.contact-template', {
        fields: ['confirmation_email', 'contact_email', 'notification_email'],
        filters: {
          'locale': locale
        }
      });

      // Escape user input
      const sanitizedData = {
        name: _.escape(data.name),
        email: _.escape(data.email),
        subject: _.escape(data.subject),
        message: _.escape(data.message),
        consent: data.consent
      };

      // Contact email
      strapi.plugins['email'].services.email.sendTemplatedEmail(
        {
          to: templates.notification_email,
          replyTo: sanitizedData.email
        },
        templates.contact_email,
        {
          data: _.pick(sanitizedData, ['name', 'email', 'subject', 'message', 'consent']),
        }
      ).then(() => {
        strapi.plugins['email'].services.email.sendTemplatedEmail(
            {
              to: sanitizedData.email,
            },
            templates.confirmation_email,
            {
              data: _.pick(sanitizedData, ['name', 'email', 'subject', 'message', 'consent']),
            }
        ).catch(() => {});;
      }).catch(() => {});
      return;
    } catch (error) {
        throw new ApplicationError('Something went wrong while sending an email with contact-mail service', { error });  
    }
  },

  sendOrderConfirmation: async (locale, data) => {
    try {
      const templates: any = await strapi.entityService.findMany('api::contact-template.contact-template', {
        fields: ['notification_email', 'order_confirmation', 'order_confirmation_admin'],
        filters: {
          'locale': locale
        }
      });

      //First send order email to admin
      //then send email to customer
      strapi.plugins['email'].services.email.sendTemplatedEmail(
        {
          to: templates.notification_email,
          replyTo: data.email
        },
        templates.order_confirmation_admin,
        {
          data: _.pick(data, ['order_id', 'order_intent_id', 'billing_name', 'amount', 'subtotal', 'shipping_name', 'shipping_line1', 'shipping_line2', 'shipping_city', 'shipping_postal_code', 'shipping_country']),
        }
      ).then(() => {
        strapi.plugins['email'].services.email.sendTemplatedEmail(
          {
            to: data.email,
          },
          templates.order_confirmation,
          {
            data: _.pick(data, ['order_id', 'order_intent_id', 'billing_name', 'amount', 'subtotal', 'shipping_name', 'shipping_line1', 'shipping_line2', 'shipping_city', 'shipping_postal_code', 'shipping_country']),
          }
      ).catch(() => {});;
      }).catch(() => {});

    } catch (error) {
      throw new ApplicationError('Something went wrong while sending an order confirmation email with contact-mail service', { error });
    }
  },

  sendGiftCard: async (locale, data) => {
    try {
      const templates: any = await strapi.entityService.findMany('api::contact-template.contact-template', {
        fields: ['notification_email', 'giftcard_confirmation', 'giftcard_confirmation_admin'],
        filters: {
          'locale': locale
        }
      });

      //First send gift card email to admin
      //then send email to recipient
      //then if needded send email to purchaser
      strapi.plugins['email'].services.email.sendTemplatedEmail(
        {
          to: templates.notification_email,
          replyTo: data.email
        },
        templates.giftcard_confirmation_admin,
        {
          data: _.pick(data, ['code', 'value', 'email', 'expiration', 'order_id', 'order_intent_id', 'billing_name']),
        }
      ).then(() => {

        if (data.send_me_copy && data.email) {
          console.log('send me copy to ', data.email); //!DEBUG
          strapi.plugins['email'].services.email.sendTemplatedEmail(
            {
              to: data.email,
            },
            templates.giftcard_confirmation,
            {
              data: _.pick(data, ['code', 'value', 'email', 'expiration', 'order_id', 'order_intent_id', 'billing_name']),
            }).catch(() => {});
        }

        if (data.send_to_recipient && data.email) {
          console.log('send to recipient to ', data.gift_card_email); //!DEBUG
          strapi.plugins['email'].services.email.sendTemplatedEmail(
            {
              to: data.gift_card_email ?? data.email,
            },
            templates.giftcard_confirmation,
            {
              data: _.pick(data, ['code', 'value', 'email', 'expiration', 'order_id', 'order_intent_id', 'billing_name']),
            }).catch(() => {});
        }
        
      }).catch(() => {});

    } catch (error) {
      throw new ApplicationError('Something went wrong while sending the gift card confirmation email with contact-mail service', { error });
    }
  },
}));
