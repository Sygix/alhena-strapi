/**
 * category service
 */

import { factories } from '@strapi/strapi';
import utils from '@strapi/utils';

const { ApplicationError } = utils.errors;

export default factories.createCoreService('api::category.category', ({ strapi }) => ({
  productsSizes: async (categoryId: number) => {
    try {
      const products: any = await strapi.entityService.findMany('api::product.product', {
        fields: ['id'],
        filters: {
          categories: categoryId,
        },
        populate: { sizes: true }
      });

      const sizesList = [...new Set(products.flatMap(item => item.sizes.map(size => size.size)).filter((size) => !size.startsWith('_')))];
      return sizesList;
    } catch (error) {
      throw new ApplicationError('Something went wrong while querying category sizes service', { error });
    }
  },
  productsColors: async (categoryId: number) => {
    try {
      const products: any = await strapi.entityService.findMany('api::product.product', {
        fields: ['id'],
        filters: {
          categories: categoryId,
        },
        populate: { colors: true }
      });

      const colorsList = [...new Set(products.flatMap(item => item.colors.map(color => color.name)).filter((color) => !color.startsWith('_')))];
      return colorsList;
    } catch (error) {
      throw new ApplicationError('Something went wrong while querying category colors service', { error });
    }
  },
}));
