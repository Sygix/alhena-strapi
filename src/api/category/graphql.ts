export const category = (strapi) => ({ nexus }) => {

    const categoryQueryTypes = nexus.extendType({
        type: 'Query',
        definition(t) {

            // "sizes" query definition
            t.field('categoryProductsSizes', {
                // Response type
                type: nexus.nonNull(nexus.list('String')),

                // Args definition
                args: { categoryId: nexus.nonNull('ID') },

                // Resolver definition
                resolve(parent, args, context) {
                    const { categoryId } = args;
                    return strapi.service('api::category.category').productsSizes(categoryId);
                }
            });

            // "colors" query definition
            t.field('categoryProductsColors', {
                // Response type
                type: nexus.nonNull(nexus.list('String')),

                // Args definition
                args: { categoryId: nexus.nonNull('ID') },

                // Resolver definition
                resolve(parent, args, context) {
                    const { categoryId } = args;
                    return strapi.service('api::category.category').productsColors(categoryId);
                }
            });

        }
    });

    return {
        types: [categoryQueryTypes],
        resolversConfig: {
            'Query.categoryProductsSizes': {
                auth: {
                    scope: ['api::category.category.productsSizes']
                }
            },
            'Query.categoryProductsColors': {
                auth: {
                    scope: ['api::category.category.productsColors']
                }
            },
        }
    };
};