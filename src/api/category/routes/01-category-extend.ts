/**
 * category-extend router
 */

const routes = [
  {
    method: 'GET',
    path: '/categories/products/sizes/:categoryId',
    handler: 'category.productsSizes',
    config: {
      policies: [],
    },
  },
  {
    method: 'GET',
    path: '/categories/products/colors/:categoryId',
    handler: 'category.productsColors',
    config: {
      policies: [],
    },
  },
  ];
  
  export default { routes };