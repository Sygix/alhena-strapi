/**
 * category controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::category.category', ({ strapi }) => ({
  async productsSizes(ctx) {
    // Getting the size id from the url params
    const { categoryId } = ctx.params;
    if (!categoryId) return ctx.badRequest('Missing \"categoryId\" in the request params');
    // Calling the custom service
    const res = await strapi.service('api::category.category').productsSizes(categoryId);
    // Sending the response
    ctx.send(res);
  },
  async productsColors(ctx) {
    // Getting the size id from the url params
    const { categoryId } = ctx.params;
    if (!categoryId) return ctx.badRequest('Missing \"categoryId\" in the request params');
    // Calling the custom service
    const res = await strapi.service('api::category.category').productsColors(categoryId);
    // Sending the response
    ctx.send(res);
  },
}));
