import { sanitize } from "@strapi/utils";

export const discount = (strapi) => ({ nexus }) => {
    const { toEntityResponse } = strapi.plugin('graphql').service('format').returnTypes;

    const discountMutationTypes = nexus.extendType({
        type: 'Mutation',
        definition(t) {

            // "generateGiftCard" mutation definition
            t.field('generateGiftCard', {
                // Response type
                type: nexus.nonNull('DiscountEntityResponse'),

                // Args definition
                args: { value: nexus.nonNull('Float'), email: nexus.nonNull('String'), expiration: nexus.nonNull('DateTime')},

                // Resolver definition
                async resolve(parent, args, context) {
                    const contentType = strapi.contentType("api::discount.discount");
                    const { value, email, expiration } = args;
                    const entities = await strapi.service('api::discount.discount').generateGiftCard(value, email, expiration);
                    const res = await sanitize.contentAPI.output(entities, contentType, {
                        auth: context.state.auth,
                    });
                    return toEntityResponse(res, { args, resourceUID: 'api::discount.discount' });
                }
            });
        }
    });

    return {
        types: [discountMutationTypes],
        resolversConfig: {
            'Mutation.generateGiftCard': {
                auth: {
                    scope: ['api::discount.discount.create']
                }
            }
        }
    };
};