/**
 * discount service
 */

import { factories } from '@strapi/strapi';
import utils from '@strapi/utils';
import crypto from 'crypto';

const { ApplicationError } = utils.errors;

export default factories.createCoreService('api::discount.discount', ({ strapi }) => ({
    updateValueLeft: async (id, value: number) => {
        const knex = strapi.db.connection;
        const transaction = await knex.transaction();

        try {
            const updateQuery = transaction('discounts').where('id', id).andWhere('value_left', '>', 0);
            const { value_left: previousValueLeft } = await transaction('discounts').select('value_left').where({ id: id }).first();

            let newValueLeft = (previousValueLeft ?? 0) - value;
            if (newValueLeft < 0) newValueLeft = 0;

            //Rounding the value_left to 2 decimal places
            const pow = Math.pow(10, 2);
            await updateQuery.update('value_left', Math.round(newValueLeft * pow) / pow);
            await transaction.commit();
        } catch (error) {
            await transaction.rollback();
            throw new ApplicationError('Something went wrong while updatingValueLeft discount service', { error });
        }
    },

    updateUsage: async (id) => {
        const knex = strapi.db.connection;
        const transaction = await knex.transaction();

        try {
            const updateQuery = transaction('discounts').where('id', id);
            const { uses: previousUsage } = await transaction('discounts').select('uses').where({ id: id }).first();
            await updateQuery.update('uses', (previousUsage ?? 0) + 1);
            await transaction.commit();
        } catch (error) {
            await transaction.rollback();
            throw new ApplicationError('Something went wrong while updatingUsage discount service', { error });
        }
    },

    generateGiftCard: async (value, email, expiration) => {
        try {
            const code = crypto.randomBytes(8).toString('hex').toUpperCase().replace(/(.{4})/g, '$1-').slice(0, -1);
            const entry = await strapi.entityService.create('api::discount.discount', {
                data: {
                    type: 'gift_card',
                    value,
                    value_left: value,
                    expiration,
                    uses: 0,
                    gift_email: email,
                    name: `gift-card_${code}`,
                    code,
                    start_date: new Date().toISOString(),
                    publishedAt: new Date().toISOString(),
                }
            });
            return entry;
        } catch (error) {
            throw new ApplicationError('Something went wrong while generating gift card', { error });
        }
    }
}));
