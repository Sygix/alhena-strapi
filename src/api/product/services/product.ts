/**
 * product service
 */

import { factories } from '@strapi/strapi';
import utils from '@strapi/utils';

const { ApplicationError } = utils.errors;

export default factories.createCoreService('api::product.product', ({ strapi }) => ({
    colors: async () => {
        try {
            const knex = strapi.db.connection;

            const colors = await knex('components_products_colors').distinct('name').select();
            const formattedColors = colors.map((entry) => entry.name).filter((color) => !color.startsWith('_'));
            
            return formattedColors;
        } catch (error) {
            throw new ApplicationError('Something went wrong while querying product colors service', { error });
        }
    },

    //Create the updateMany custom service
    updateMany: async (productIds = [], datas = [{}], locale: string) => {
        const knex = strapi.db.connection;
        const transaction = await knex.transaction();

        try {
            let returning = [];
            for (let i = 0; i < productIds.length; i++){
                const updateQuery = transaction('products').where('id', productIds[i]).andWhere('locale', locale);

                if (datas[i].hasOwnProperty('global_quantity')) {
                    const { global_quantity } = await transaction('products').select('global_quantity').where({ id: productIds[i] }).first();
                    const dataQty = datas[i]['global_quantity'];
                    updateQuery.andWhere('global_quantity', '>', -1);
                    if ((global_quantity + dataQty) < 0) continue;
                    if (dataQty < 0) updateQuery.andWhere('global_quantity', '>=', dataQty);
                    datas[i]['global_quantity'] = global_quantity + dataQty;
                }
        
                const res = await updateQuery.update(datas[i]).returning('*');
                returning = [...returning, ...res];
            }
            await transaction.commit();
            return returning;
        } catch (error) {
            await transaction.rollback();
            throw new ApplicationError('Something went wrong while updating product-size service', { error: error });
        }
    }
}));