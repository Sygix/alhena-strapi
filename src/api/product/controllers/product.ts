/**
 * product controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::product.product', ({ strapi }) => ({
    async colors(ctx) {
        const colors = await strapi.service('api::product.product').colors();
        // Sending the response
        ctx.send(colors);
    },
}));
