export const product = (strapi) => ({ nexus }) => {

    const productQueryTypes = nexus.extendType({
        type: 'Query',
        definition(t) {

            // "sizes" query definition
            t.field('sizes', {
                // Response type
                type: nexus.nonNull(nexus.list('String')),

                // Args definition
                args: {},

                // Resolver definition
                resolve(parent, args, context) {
                    return strapi.service('api::product.product-size').sizes();
                }
            });

            // "colors" query definition
            t.field('colors', {
                // Response type
                type: nexus.nonNull(nexus.list('String')),

                // Args definition
                args: {},

                // Resolver definition
                resolve(parent, args, context) {
                    return strapi.service('api::product.product').colors();
                }
            });

        }
    });

    const productMutationTypes = nexus.extendType({
        type: 'Mutation',
        definition(t) {

            // "update" mutation definition
            t.field('updateProductSize', {
                // Response type
                type: nexus.nonNull('ComponentProductsSizes'),

                // Args definition
                args: { id: nexus.nonNull('ID'), data: nexus.nonNull('ComponentProductsSizesInput') },

                // Resolver definition
                resolve(parent, args, context) {
                    const { id, data } = args;
                    return strapi.service('api::product.product-size').update(id, data);
                }
            });

            // "updateMany" mutation definition
            t.field('updateManyProductSize', {
                // Response type
                type: nexus.list(nexus.nonNull('ComponentProductsSizes')),

                // Args definition
                args: { ids: nexus.list(nexus.nonNull('ID')), datas: nexus.list(nexus.nonNull('ComponentProductsSizesInput')) },

                // Resolver definition
                resolve(parent, args, context) {
                    const { ids, datas } = args;
                    return strapi.service('api::product.product-size').updateMany(ids, datas);
                }
            });

            // "updateMany" mutation definition
            t.field('updateManyProducts', {
                // Response type
                type: nexus.list(nexus.nonNull('ProductEntity')),

                // Args definition
                args: { ids: nexus.list(nexus.nonNull('ID')), datas: nexus.list(nexus.nonNull('ProductInput')), locale: nexus.nonNull('I18NLocaleCode') },

                // Resolver definition
                resolve(parent, args, context) {
                    const { ids, datas, locale } = args;
                    return strapi.service('api::product.product').updateMany(ids, datas, locale);
                }
            });
        }
    });

    return {
        types: [productQueryTypes, productMutationTypes],
        resolversConfig: {
            'Query.sizes': {
                auth: {
                    scope: ['api::product.product-size.sizes']
                }
            },
            'Query.colors': {
                auth: {
                    scope: ['api::product.product.colors']
                }
            },
            'Mutation.updateProductSize': {
                auth: {
                    scope: ['api::product.product-size.update']
                }
            },
            'Mutation.updateManyProductSize': {
                auth: {
                    scope: ['api::product.product-size.updateMany']
                }
            },
            'Mutation.updateManyProducts': {
                auth: {
                    scope: ['api::product.product.update']
                }
            }
        }
    };
};