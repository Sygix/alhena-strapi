/**
 * product-size router
 */

const routes = [
  {
    method: 'GET',
    path: '/products/sizes',
    handler: 'product-size.sizes',
    config: {
      policies: [],
    },
  },
  {
    method: 'GET',
    path: '/products/colors',
    handler: 'product.colors',
    config: {
      policies: [],
    },
  },
  {
    method: 'PUT',
    path: '/product-size/:sizeId',
    handler: 'product-size.update',
    config: {
      policies: [],
    },
  },
  {
    method: 'PUT',
    path: '/product-sizes/',
    handler: 'product-size.updateMany',
    config: {
      policies: [],
    },
  },
];

export default { routes };