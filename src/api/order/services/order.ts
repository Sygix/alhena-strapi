/**
 * order service
 */

import { factories } from '@strapi/strapi';
import utils from '@strapi/utils';
import Stripe from 'stripe';

const { ApplicationError, ForbiddenError, UnauthorizedError } = utils.errors;

//TODO: Cleaning service will also need to clean non stripe orders

export default factories.createCoreService('api::order.order', ({ strapi }) => ({
    //Creating the cleaning custom service
    //This will remove unused order and release ressources
    clean: async (interval: number) => {
        const setting: any = await strapi.entityService.findOne('api::setting.setting', 1, {
            fields: ['stripe_secret_key'],
        });

        try {
            const stripe = new Stripe(setting.stripe_secret_key, { apiVersion: '2022-11-15' });
            const todayMinusInterval = new Date();
            todayMinusInterval.setDate(interval);

            const entries: any = await strapi.entityService.findMany('api::order.order', {
                fields: ['payment_intent_id', 'status', 'products'],
                filters: {
                    $or: [
                        {
                        status: 'pending'
                        },
                        {
                        status: 'checkout'
                        }
                    ],
                    updatedAt: {
                        $lt: todayMinusInterval.toISOString()
                    }
                }
            });

            entries.forEach(async entrie => {
                if (entrie.status === 'pending' && entrie.payment_intent_id) {
                    //cancel order with stripe
                    //change status to canceled then restock the product
                    try {
                        const res = await stripe.paymentIntents.cancel(entrie.payment_intent_id, { cancellation_reason: 'abandoned' });
                        if (res.status === 'canceled') {
                            await strapi.entityService.update<any, any>('api::order.order', entrie.id, {
                                data: {
                                    status: 'canceled',
                                }
                            });
                            let productIds: number[] = [];
                            let productQty: any[] = [];
                            entrie.products.forEach(async product => {
                                await strapi.service('api::product.product-size').update(product.sizeId, {
                                    quantity: product.qty
                                });
                                productIds.push(product.id);
                                productQty.push({
                                    global_quantity: product.qty
                                });
                            });
                            await strapi.service('api::product.product').updateMany(productIds, productQty, entrie.locale);
                        }
                    } catch (error) {
                        if (error.type === 'StripeInvalidRequestError') {
                            const status = error.raw.payment_intent?.status;
                            await strapi.entityService.update<any, any>('api::order.order', entrie.id, {
                                data: {
                                    status,
                                }
                            });
                            if (status === 'canceled') {
                                let productIds: number[] = [];
                                let productQty: any[] = [];
                                entrie.products.forEach(async product => {
                                    await strapi.service('api::product.product-size').update(product.sizeId, {
                                        quantity: product.qty
                                    });
                                    productIds.push(product.id);
                                    productQty.push({
                                        global_quantity: product.qty
                                    });
                                });
                                await strapi.service('api::product.product').updateMany(productIds, productQty, entrie.locale);
                            }
                        }
                    }
                } else if (entrie.status === 'checkout') {
                    // delete the order entry
                    // cancel the payment intent
                    if(entrie.payment_intent_id) await stripe.paymentIntents.cancel(entrie.payment_intent_id, { cancellation_reason: 'abandoned' });
                    await strapi.entityService.delete('api::order.order', entrie.id);
                }
            });
        } catch (error) {
            throw new ApplicationError('Something went wrong while cleaning orders service', { error: error });
        }
    },

    //Creating the update custom service
    updateByOrderIntentId: async (order_id: number, order_intent_id: string, data: any) => {
        try {
            const savedOrder: any = await strapi.entityService.findOne('api::order.order', order_id, {
                fields: ['order_intent_id', 'discount_id', 'status'],
            });

            if(savedOrder.status !== 'pending' && savedOrder.status !== 'checkout') throw new UnauthorizedError('The status of this order prevent it from being updated');

            if (data.email && (savedOrder.discount_id || data.discount_id)) {
                const relatedDiscount: any = await strapi.entityService.findOne('api::discount.discount', data.discount_id ?? savedOrder.discount_id, {
                    fields: ['gift_email', 'type'],
                });
                if(relatedDiscount.type === 'gift_card' && (relatedDiscount.gift_email !== data.email)) {
                    throw new ForbiddenError('Incorrect discount email');
                }
            }

            if (savedOrder.order_intent_id === order_intent_id) {
                const entry = await strapi.entityService.update('api::order.order', order_id, {
                    data: data
                });

                return entry;
            } else {
                throw new ForbiddenError('Incorrect order_intent_id');
            }
        } catch (error) {
            throw new ApplicationError('Something went wrong while updateByOrderIntentId order service', { error: error });
        }
    }
}));
