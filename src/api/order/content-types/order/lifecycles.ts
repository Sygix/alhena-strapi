import * as crypto from 'crypto';

export default {
    beforeCreate(event) {
        const { data } = event.params;
        data.order_intent_id = crypto.randomUUID();
    },

    async afterUpdate(event) {
        const { discount_id, status, amount, subtotal, products } = event.result;
        
        if (status === 'succeeded' && discount_id) {
            const pow = Math.pow(10, 2);
            //Rounding the discount amount to 2 decimal places
            const discount_amount = Math.round((amount - subtotal) * pow) / pow;
            strapi.service('api::discount.discount').updateValueLeft(discount_id, discount_amount);
            strapi.service('api::discount.discount').updateUsage(discount_id);
        };

        if (status === 'succeeded' && products.find((product) => product.gift_card)) {
            const setting: any = await strapi.entityService.findOne('api::setting.setting', 1, {
                fields: ['gift_card_expiration'],
            });
            const expiration = new Date().setMonth(new Date().getMonth() + parseInt(setting.gift_card_expiration));
            const giftCardProducts = products.filter((product) => product.gift_card);

            for (const product of giftCardProducts) {
                for (let i = 0; i < product.qty; i++){
                    const gift_card = await strapi.service('api::discount.discount').generateGiftCard(product.price, event.result.gift_card_email ?? event.result.email, expiration);
                    strapi.service('api::contact-template.contact-mail').sendGiftCard(event.result.locale ?? 'fr', {
                        send_to_recipient: event.result.send_to_recipient,
                        send_me_copy: event.result.send_me_copy,
                        gift_card_email: gift_card.gift_email,
                        email: event.result.email,
                        code: gift_card.code ?? 'N/A',
                        value: gift_card.value ?? 'N/A',
                        expiration: gift_card.expiration ?? 'N/A',
                        order_id: event.result.id ?? 'N/A',
                        order_intent_id: event.result.order_intent_id ?? 'N/A',
                        billing_name: event.result.billing_name ?? 'N/A',
                    });
                }
            }
        }

        if (status === 'succeeded') {
            strapi.service('api::contact-template.contact-mail').sendOrderConfirmation(event.result.locale ?? 'fr', {
                email: event.result.email,
                order_id: event.result.id ?? 'N/A',
                order_intent_id: event.result.order_intent_id ?? 'N/A',
                billing_name: event.result.billing_name ?? 'N/A',
                amount: event.result.amount ?? 'N/A',
                subtotal: event.result.subtotal ?? 'N/A',
                shipping_name: event.result.shipping_name ?? event.result.billing_name ?? 'N/A',
                shipping_line1: event.result.shipping_line1 ?? event.result.billing_line1 ?? 'N/A',
                shipping_line2: event.result.shipping_line2 ?? event.result.billing_line2 ?? 'N/A',
                shipping_city: event.result.shipping_city ?? event.result.billing_city ?? 'N/A',
                shipping_postal_code: event.result.shipping_postal_code ?? event.result.billing_postal_code ?? 'N/A',
                shipping_country: event.result.shipping_country ?? event.result.billing_country ?? 'N/A',
            });
        }
    }
};