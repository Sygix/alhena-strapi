import { sanitize, validate } from "@strapi/utils";

export const order = (strapi) => ({ nexus }) => {
    const { toEntityResponse } = strapi.plugin('graphql').service('format').returnTypes;

    const orderMutationTypes = nexus.extendType({
        type: 'Mutation',
        definition(t) {

            // "updateByOrderIntentId" mutation definition
            t.field('updateByOrderIntentId', {
                // Response type
                type: nexus.nonNull('OrderEntityResponse'),

                // Args definition
                args: { id: nexus.nonNull('ID'), order_intent_id: nexus.nonNull('String'), data: nexus.nonNull('OrderInput') },

                // Resolver definition
                async resolve(parent, args, context) {
                    const contentType = strapi.contentType("api::order.order");
                    const { id, order_intent_id, data } = args;
                    const entities = await strapi.service('api::order.order').updateByOrderIntentId(id, order_intent_id, data);
                    const res = await sanitize.contentAPI.output(entities, contentType, {
                        auth: context.state.auth,
                    });
                    return toEntityResponse(res, { args, resourceUID: 'api::order.order' });
                }
            });
        }
    });

    return {
        types: [orderMutationTypes],
        resolversConfig: {
            'Mutation.updateByOrderIntentId': {
                auth: {
                    scope: ['api::order.order.update']
                }
            }
        }
    };
};