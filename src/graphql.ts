import { category } from "./api/category/graphql";
import { contactMail } from "./api/contact-template/graphql";
import { discount } from "./api/discount/graphql";
import { order } from "./api/order/graphql";
import { product } from "./api/product/graphql";

const extensions = [product, contactMail, category, order, discount];

const graphql = (strapi) => {
    const extensionService = strapi.plugin('graphql').service('extension');

    for (const extension of extensions) {
        extensionService.use(extension(strapi));
    };
}

export default graphql;